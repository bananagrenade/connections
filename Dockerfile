FROM openjdk:17
EXPOSE 8080
COPY build/libs/connections-*.jar /app/app.jar
CMD ["java", "-jar", "/app/app.jar"]
